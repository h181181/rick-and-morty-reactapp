import React from "react";
import "./App.css";
import "./containers/CharacterCards/CardsPage";
import "./containers/LocationCards/LocationsPage";

import Toolbar from "./components/Toolbar/Toolbar";

function App(props) {
	return (
		<div className="App">
			<header className="App-header">
				<div className="imageDiv">
					<img
						src={
							"https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
						}
						alt="banner"
						className="img-responsive"
					></img>
				</div>
				<Toolbar />
				<div className="CardsPage">
					<div> {props.children} </div>
				</div>
			</header>
		</div>
	);
}

export default App;
