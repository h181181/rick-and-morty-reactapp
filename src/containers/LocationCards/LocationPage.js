import React from "react";
import LocationCard from "../../components/CharacterCard/CharacterCard";

//Create a new state with one property called character

class LocationPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: props.match.params.id,
			character: {}
		};
	}

	//Write a new ES6 arrow function called getLocationData
	getLocationData() {
		fetch("https://rickandmortyapi.com/api/location/ " + this.state.id) //location endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ location: data }); //update the location property
			})
			.catch(error => {
				console.log(error);
			});
	}
	componentDidMount() {
		this.getLocationData();
	}

	render() {
		let locationCard = null;
		if (this.state.location.id) {
			locationCard = (
				<div>
					<h1>Heres more info</h1>
					<LocationCard key={this.state.location.id} {...this.state.location}></LocationCard>
				</div>
			);
		} else {
			locationCard = <p>Loading...</p>;
		}
		return <React.Fragment>{locationCard}</React.Fragment>;
	}
}
export default LocationPage;
