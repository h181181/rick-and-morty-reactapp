import React from "react";
import LocationCard from "./../../components/LocationCard/LocationCard";
import SearchLocation from "../../components/SearchLocation/SearchLocation";

//state as an Object with two properties
class LocationsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rickMorty: [],
			locationCards: []
		};
		this.getSearchLocations = this.getSearchLocations.bind(this);
	}

	//ES6 Arrow function called getData()
	getData() {
		fetch("https://rickandmortyapi.com/api/location/") //API endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ locationCards: data.results });
			})
			.catch(error => {
				console.log(error);
			});
	}

	componentDidMount() {
		this.getData();
	}

	render() {
		let locations = null;
		if (this.state.locationCards.length > 0) {
			locations = this.state.locationCards.map(location => (
				<LocationCard key={location.id} {...location} showLink={true}></LocationCard>
			));
		} else {
			locations = <p>Loading...</p>;
		}
		return (
			<React.Fragment>
				<h1>Ricky and Morty Locations</h1>
				<SearchLocation onSearch={this.getSearchLocations} />
				<br />
				<div class="container">
					<div class="row" id="row"></div>
				</div>
				<br />
				<div className="row">{locations}</div>
			</React.Fragment>
		);
	}

	//Search function start
	getSearchLocations(input) {
		fetch(`https://rickandmortyapi.com/api/location/?name=${input}`) //API endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ locationCards: data.results });
			})
			.catch(error => {
				console.log(error);
			});
		console.log(input);
	}
}

export default LocationsPage;
