import React from "react";
import CharacterCard from "./../../components/CharacterCard/CharacterCard";
import SearchCharacter from "../../components/SearchCharacter/SearchCharacter";

//state as an Object with two properties
class CardsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rickMorty: [],
			characterCards: []
		};
		this.getSearchCharacters = this.getSearchCharacters.bind(this);
	}

	//ES6 Arrow function called getData()
	getData() {
		fetch("https://rickandmortyapi.com/api/character/") //API endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ characterCards: data.results });
			})
			.catch(error => {
				console.log(error);
			});
		//Use the built-in React function .setState() to update our rickMorty
		//array declared in our Component State
	}

	componentDidMount() {
		this.getData();
	}

	render() {
		let characters = null;
		if (this.state.characterCards.length > 0) {
			characters = this.state.characterCards.map(character => (
				<CharacterCard key={character.id} {...character} showLink={true}></CharacterCard>
			));
		} else {
			characters = <p>Loading...</p>;
		}
		return (
			<React.Fragment>
				<h1>Ricky and Morty Characters</h1>
				<SearchCharacter onSearch={this.getSearchCharacters} />
				<br />
				<div class="container">
					<div class="row" id="row"></div>
				</div>
				<br />
				<div className="row">{characters}</div>
			</React.Fragment>
		);
	}

	//Search function start
	getSearchCharacters(input) {
		fetch(`https://rickandmortyapi.com/api/character/?name=${input}`) //API endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ characterCards: data.results });
			})
			.catch(error => {
				console.log(error);
			});
		console.log(input);
	}
}

export default CardsPage;
