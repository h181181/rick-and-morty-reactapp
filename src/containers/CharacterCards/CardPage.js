import React from "react";
import CharacterCard from "../../components/CharacterCard/CharacterCard";

//Create a new state with one property called character

class CardPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: props.match.params.id,
			character: {}
		};
	}
	//Write a new ES6 arrow function called getCharacterData
	getCharacterData() {
		fetch("https://rickandmortyapi.com/api/character/ " + this.state.id) //character endpoint
			.then(response => response.json())
			.then(data => {
				this.setState({ character: data }); //update the character property
			})
			.catch(error => {
				console.log(error);
			});
	}
	componentDidMount() {
		this.getCharacterData();
	}

	render() {
		let characterCard = null;
		if (this.state.character.id) {
			characterCard = (
				<div>
					<h1>Heres your character</h1>
					<CharacterCard key={this.state.character.id} {...this.state.character}></CharacterCard>
				</div>
			);
		} else {
			characterCard = <p>Loading...</p>;
		}
		return <React.Fragment>{characterCard}</React.Fragment>;
	}
}
export default CardPage;
