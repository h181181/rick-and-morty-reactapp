import React from "react";

class SearchLocation extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ""
		};

		this.handleClick = this.handleClick.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
	}

	render() {
		return (
			<div className="searchBar">
				<b>Search for a Rick and Morty locations</b>
				<form className="form-control">
					<input type="text" id="search" placeholder="Search now" onChange={this.handleSearch} />
					<button type="button" id="button" onClick={this.handleClick}>
						Search
					</button>
				</form>
			</div>
		);
	}

	handleClick() {
		if (this.props.onSearch) {
			this.props.onSearch(this.state.search);
		}
	}

	handleSearch(e) {
		this.setState({
			search: e.target.value
		});
	}
}

export default SearchLocation;
