import React from "react";
import { NavLink } from "react-router-dom";

const locationCard = props => {
	const link = props.showLink ? (
		<NavLink to={"/locations/" + props.id}>View locations</NavLink>
	) : null;
	return (
		<div className="col-xs-12 col-sm-6 col-md-4">
			<div className="card LocationCard">
				<div className="card-body" style={{ marginBottom: "40px" }}>
					<h3 className="card-title">{props.name}</h3>
					<b>Type: </b> {props.type} <br />
					<b>Dimension: </b> {props.dimension} <br />
					<b>URL: </b> {props.url} <br />
					<b>Created</b> {props.created} <br />
					<br></br>
				</div>
			</div>
		</div>
	);
};

//Export your const locationCard as the default
export default locationCard;
