import React from "react";
import { NavLink } from "react-router-dom";

//Return valid JSX in the function
const characterCard = props => {
	const link = props.showLink ? <NavLink to={"/character/" + props.id}>View more</NavLink> : null;
	return (
		<div className="col-xs-12 col-sm-6 col-md-4">
			<div className="card CharacterCard">
				<img src={props.image} alt={props.name} className="card-img-top" />
				<div className="card-body">
					<h3 className="card-title">{props.name}</h3>
					<b>Species: </b> {props.species} <br />
					<b>Status: </b> {props.status} <br />
					<b>Gender: </b> {props.gender} <br />
					<b>Location: </b> {props.location.name} <br />
					<b>Place of origin</b> {props.origin.name} <br />
					<b>{link}</b>
					<br></br>
				</div>
			</div>
		</div>
	);
};

//Export your const characterCard as the default
export default characterCard;
