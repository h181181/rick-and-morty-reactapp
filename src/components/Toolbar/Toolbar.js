import React, { Component } from "react";

import { Link } from "react-router-dom";

class Toolbar extends Component {
	constructor(props, ...rest) {
		super(props, ...rest);
		this.state = {};
	}

	render() {
		return (
			<nav style={{ width: "100%", height: "60px", background: "#555" }}>
				<Link to="/" style={{ marginRight: "20px" }}>
					Characters
				</Link>
				<Link to="/location">Locations</Link>
			</nav>
		);
	}
}

export default Toolbar;
