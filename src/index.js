import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import CardsPage from "./containers/CharacterCards/CardsPage";
import CardPage from "./containers/CharacterCards/CardPage";
import LocationPage from "./containers/LocationCards/LocationPage";
import LocationsPage from "./containers/LocationCards/LocationsPage";

ReactDOM.render(
	<BrowserRouter>
		<App>
			<Route path="/character/:id" component={CardPage} />
			<Route exact path="/" component={CardsPage} />
			<Route path="/location/:id" component={LocationPage} />
			<Route path="/location" component={LocationsPage} />
		</App>
	</BrowserRouter>,
	document.getElementById("root")
);

serviceWorker.unregister();
